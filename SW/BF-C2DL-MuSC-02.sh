#!/bin/bash
# stores result for the "BF-C2DL-MuSC" sequences

rm -rf '../BF-C2DL-MuSC/02_RES'
cd deepwater || exit
python3 segment.py --name BF-C2DL-MuSC --config_path .. --data_path ../.. --seq 02
