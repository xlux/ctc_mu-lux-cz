#!/bin/bash
# stores result for the "PhC-C2DL-PSC" sequences

rm -rf '../PhC-C2DL-PSC/02_RES'
cd deepwater || exit
python3 segment.py --name PhC-C2DL-PSC --config_path .. --data_path ../.. --seq 02
