#!/bin/bash
# stores result for the "Fluo-N2DH-SIM+" sequences

rm -rf '../Fluo-N2DH-SIM+/01_RES'
cd deepwater || exit
python3 segment.py --name Fluo-N2DH-SIM+ --config_path .. --data_path ../.. --seq 01
