#!/bin/bash
# stores result for the "DIC-C2DH-HeLa" sequences

rm -rf '../DIC-C2DH-HeLa/01_RES'
cd deepwater || exit
python3 segment.py --name DIC-C2DH-HeLa --config_path .. --data_path ../.. --sequence 01


