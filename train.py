import sys
import os

if __name__ == '__main__':
    cwd = os.getcwd()
    sys.path.insert(0, os.path.join(cwd, 'SW', 'deepwater'))
    print(sys.path)

    import SW.deepwater.deepwater as dw
    dw.deepwater(mode=1)
